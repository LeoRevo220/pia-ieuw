INSERT INTO Ingredientes(Nombre)VALUES('Peperoni'),('Carne Bolo�esa'), ('Jamon'), ('Champi�ones'),('Aceituna'), ('Atun'),('Atun'),('Tocino'), ('Salami'),('Pi�a'),('Salchicha Italiana'), ('Salchicha de Pavo')
,('Cebolla'), ('Pimiento Verde'), ('Jalape�o'), ('Chorizo');


INSERT INTO Articulo(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza con pan sencillo y orilla regular, con riquisimo peperonni', 'Carnivora', 'PizzaEspecial', 4);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Suprema', 'PizzaEspecial', 5);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Porkys', 'PizzaEspecial', 3);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Olgas', 'PizzaEspecial', 3);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Italiana', 'PizzaEspecial', 3);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Norte�a', 'PizzaEspecial', 4);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Del Patron', 'PizzaEspecial', 6);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(149, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella', 'Del Inge', 'PizzaEspecial', 3);

UPDATE Articulos SET ImagenID = 1 WHERE ID = 1;

INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(3,1),(9,1),(1,1),(12,1);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(3,2),(1,2),(11,2),(14,2),(13 ,2);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(3,3),(9,3),(8,3);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(3,4),(16,4),(10,4);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(14,5),(1,5),(4,5);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(2,6),(16,6),(15,6), (13,6);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(3,7),(9,7),(1,7), (12,7), (2,7), (14,7);
INSERT INTO IngredientePizza(IngredientesID, PizzasID)VALUES(6,8),(5,8),(15,8);



-- PIZZAS SENCILLAS MODIFICABLES

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella. 1 Ingrediente', 'Pizza Extra Grande 14"', 'Pizza', 1);

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(169, 'Pizza grande tradicional, con el doble de todo. 1 Ingrediente', 'Pizza Chicago Style Extra Grande 14"', 'Pizza', 1);



INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(35, 'Espaguetti ba�ado de salsa italiana roja', 'Espaguetti Italiano', 'Espagueti');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(55, 'Espaguetti ba�ado con salsa italiana roja, champi�ones, carne y queso', 'Espaguetti Mutcha', 'Espagueti');


INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(49, 'Espaguetti ba�ado con salsa italiana, y queso Gouda', 'Espaguetti Gratinado', 'Espagueti');


INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(39, 'Papas en gajo', 'Papas', 'Complemento');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(55, 'Papas en gajo ba�adas con queso amarillo y tocino bits', 'Papas Nachos', 'Complemento');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(99, 'Boneless de pollo, 250 gramos', 'Boneless', 'Complemento');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(29, 'Roles de canela ba�ados con rico glaseado y jarabe de chocolate', 'Cannerroles', 'Complemento');


INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(22, 'Refresco Coca-Cola sabor original 500ml', 'Coca-Cola Original 500ml', 'Bebida');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(25, 'Refresco Fuze Tea sabor limon 500ml', 'Fuze Tea Limon 500ml', 'Bebida');


INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(22, 'Refresco Fanta sabor naranja 500ml', 'Fanta Naranja 500ml', 'Bebida');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(22, 'Refresco Joya sabor manzana 500ml', 'Joya Manzana 500ml', 'Bebida');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(22, 'Refresco Sprite sabor lima-limon 500ml', 'Sprite Lima-Limon 500ml', 'Bebida');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(23, 'Refresco Topo Chico sabor uva 500ml', 'Topo Chico Sangr�a 600ml', 'Bebida');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(20, 'Agua Natural Ciel 500ml', 'Agua Ciel 600ml', 'Bebida');

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(39, 'Refresco Coca-Cola sabor orginal 2lt', 'Coca-Cola Original 2lt', 'Bebida');


--1
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\pizzafromtop1.png', Single_Blob) as img

--2
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza1.png', Single_Blob) as img

UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza1.jpg', Single_Blob) as img) WHERE ImagenID = 2;

--3
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza2.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza2.jpg', Single_Blob) as img) WHERE ImagenID = 3;
--4
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza3.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza3.jpg', Single_Blob) as img) WHERE ImagenID = 4;

--5
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza4.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza4.jpg', Single_Blob) as img) WHERE ImagenID = 5;

--6
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza5.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza5.jpg', Single_Blob) as img) WHERE ImagenID = 6;


--7
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza6.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza6.jpg', Single_Blob) as img) WHERE ImagenID = 7;
--8
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza7.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza7.jpg', Single_Blob) as img) WHERE ImagenID = 8;

--9
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza8.png', Single_Blob) as img


UPDATE Imagenes SET ImagenData = (SELECT BulkColumn FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza8.jpg', Single_Blob) as img) WHERE ImagenID = 9;

UPDATE Articulos SET ImagenID = 2 WHERE ID = 2;
UPDATE Articulos SET ImagenID = 3 WHERE ID = 3;
UPDATE Articulos SET ImagenID = 4 WHERE ID = 4;
UPDATE Articulos SET ImagenID = 5 WHERE ID = 5;
UPDATE Articulos SET ImagenID = 6 WHERE ID = 6;
UPDATE Articulos SET ImagenID = 7 WHERE ID = 7;
UPDATE Articulos SET ImagenID = 8 WHERE ID = 8;
UPDATE Articulos SET ImagenID = 9 WHERE ID = 9;

--10 REFRESCOS
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\aguaCiel.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 10 WHERE ID = 24;

--11
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\coca.png', Single_Blob) as img

UPDATE Articulos SET ImagenID = 11 WHERE ID = 18;

--12
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\coca2lt.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 12 WHERE ID = 25;

--13
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\fanta.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 13 WHERE ID = 20;

--14
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\fuzeTea.webp', Single_Blob) as img

UPDATE Articulos SET ImagenID = 14 WHERE ID = 19;


--15
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\joyaManzana.png', Single_Blob) as img

UPDATE Articulos SET ImagenID = 15 WHERE ID = 21;

--16
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\sangria.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 16 WHERE ID = 23;


--17
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\sodas\sprite.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 17 WHERE ID = 22;

--18 ESPAGUETTIS
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\spaghetti.webp', Single_Blob) as img
UPDATE Articulos SET ImagenID = 18 WHERE ID = 11;

--19
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\spaghetti2.webp', Single_Blob) as img

UPDATE Articulos SET ImagenID = 19 WHERE ID = 12;

--20

insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\spaghett3.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 20 WHERE ID = 13;


--21 COMPLEMENTOS

insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\papas.jpg', Single_Blob) as img
UPDATE Articulos SET ImagenID = 21 WHERE ID = 14;
--22
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\papasConTocino.jpg', Single_Blob) as img


UPDATE Articulos SET ImagenID = 22 WHERE ID = 15;
--23
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\boneless.webp', Single_Blob) as img

UPDATE Articulos SET ImagenID = 23 WHERE ID = 16;

--24
insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\01FCFM\6TO\INTERFACE\PIA\DotnetCore\PIAInterfaz\PIAInterfaz\wwwroot\img\canela.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 24 WHERE ID = 17;




--25 PIZZAS BASICA

insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza10.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = 25 WHERE ID = 10;







--A�ADIENDO COMPRAS
INSERT INTO Compra(UsuarioId, Fecha, PrecioTotal)VALUES('fa164055-51cc-4fed-afe5-6d8e1761dc33', GetDate(), 500);
INSERT INTO ArticuloCompra(ArticulosID, ComprasID)VALUES(1,1);

INSERT INTO Compra(UsuarioId, Fecha, PrecioTotal)VALUES('fa164055-51cc-4fed-afe5-6d8e1761dc33', GetDate(), 600);
INSERT INTO ArticuloCompra(ArticulosID, ComprasID)VALUES(2,SCOPE_IDENTITY());


INSERT INTO ArticulosVendidos(Precio, Nombre, ArticuloID)VALUES(149, 'Carnivora', 1);
INSERT INTO Compra(UsuarioId, Fecha, PrecioTotal)VALUES('fa164055-51cc-4fed-afe5-6d8e1761dc33', GetDate(), 750);
UPDATE ArticulosVendidos SET CompraID = 5 WHERE ArticuloVendidoID = 1;


INSERT INTO ArticulosVendidos(Precio, Nombre, ArticuloID)VALUES(149, 'Porkys', 3);
INSERT INTO ArticulosVendidos(Precio, Nombre, ArticuloID)VALUES(149, 'Suprema', 2);
INSERT INTO ArticulosVendidos(Precio, Nombre, ArticuloID)VALUES(149, 'Suprema', 2);
INSERT INTO Compra(UsuarioId, Fecha, PrecioTotal)VALUES('ded622b2-155a-45d9-9d98-07044d0d39b0', GetDate(), 750);
UPDATE ArticulosVendidos SET CompraID = 6 WHERE ArticuloVendidoID = 2;
UPDATE ArticulosVendidos SET CompraID = 6 WHERE ArticuloVendidoID = 3;
UPDATE ArticulosVendidos SET CompraID = 6 WHERE ArticuloVendidoID = 4;







--A�ADIENDO PROMOCIONES

--PROMOCION 2x2
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 2);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 2);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();

UPDATE Articulos SET PromocionID = 33 WHERE ID = 26;
UPDATE Articulos SET PromocionID = 33 WHERE ID = 27;

insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza12.jpg', Single_Blob) as img


UPDATE Articulos SET ImagenID = SCOPE_IDENTITY() WHERE ID = 33;

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(199, '2 Pizzas grandes tradicionales de 2 ingredientes.', 'Duo de Dos', 'Promocion');


--PROMOCION 3x3
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 3);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 3);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 3);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();


UPDATE Articulos SET PromocionID = 34 WHERE ID = 28;
UPDATE Articulos SET PromocionID = 34 WHERE ID = 29;
UPDATE Articulos SET PromocionID = 34 WHERE ID = 30


insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza11.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = SCOPE_IDENTITY() WHERE ID = 34;

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(299, '3 Pizzas grandes tradicionales de 3 ingredientes.', 'Trio de Tres', 'Promocion');

--PROMOCION 2x169
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 1);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();
INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator, CantidadDeIngredientesMinimos)VALUES
(99, 'Pizza grande tradicional, original con italianisima salsa y queso mozarella.', 'Pizza Extra Grande 14"', 'Pizza', 1);
UPDATE Articulos SET ImagenID = 1 WHERE ID = SCOPE_IDENTITY();


UPDATE Articulos SET PromocionID = 35 WHERE ID = 31;
UPDATE Articulos SET PromocionID = 35 WHERE ID = 32;


insert into Imagenes(ImagenData) 
SELECT BulkColumn 
FROM Openrowset( Bulk 'D:\Users\Felix\Documents\websiteimages\pizzas\pizza13.jpg', Single_Blob) as img

UPDATE Articulos SET ImagenID = SCOPE_IDENTITY() WHERE ID = 35;

INSERT INTO Articulos(Precio, Descripcion, Nombre, Discriminator)VALUES
(169, '2 Pizzas grandes tradicionales de 1 ingrediente.', '2 Pizzas X $169', 'Promocion');



SELECT * FROM Articulos;

UPDATE Articulos SET Activo = 0 WHERE ID >= 26 AND ID <= 32;