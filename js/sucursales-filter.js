$('document').ready(function(){
    
    // alert("ya cargaron todos");
    $('.sucursal').hide();
    filterSucursales();
    $('#municipio-filter').change(function(){
        filterSucursales();
        // $('iframe').load(function(){
            //     alert("Se ha cargado el mapa");
            // });
            
    });
        
    function filterSucursales(){
        $('.sucursal').hide();
        let currentMunicipio = $('#municipio-filter').val();
        let textMunicipio = $('#municipio-filter option:selected').text();
        let sucursalesFiltered = $('.sucursal').filter('.'+currentMunicipio);
        let numberOfSucursales = sucursalesFiltered.length;
        let message  = ' Sucursales';
        sucursalesFiltered.css('width', '48%');
        if(numberOfSucursales == 1){
            message = ' Sucursal'
        } else if(numberOfSucursales == 3) {
            sucursalesFiltered.css('width', '30%');
        }


        $('h1').text(textMunicipio + ' - ' + numberOfSucursales + message);
        sucursalesFiltered.show();
    }


    
});