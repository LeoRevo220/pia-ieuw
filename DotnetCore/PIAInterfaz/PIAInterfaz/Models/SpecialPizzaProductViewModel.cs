﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class SpecialPizzaProductViewModel
    {
        public PizzaEspecialViewModel Pizza { get; set; }

        public List<PizzaEspecialViewModel> ArticulosRelacionados { get; set; }
    }
}
