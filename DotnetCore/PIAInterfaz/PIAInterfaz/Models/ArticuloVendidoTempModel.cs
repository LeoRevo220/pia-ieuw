﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class ArticuloVendidoTempModel
    {
        public Guid ArticuloEnCarritoID { get; set; }
        public int ArticuloVendidoID { get; set; }

        //public float Precio { get; set; }

        public string Nombre { get; set; }

        public int ImagenID { get; set; }

        public int Cantidad { get; set; }

        public float Precio { get; set; }

        public float Subtotal { get; set; }

        public string TipoDePan { get; set; }

        public List<string> TipoDePanesArticulosPromocion { get; set; }

        public string TipoDeArticulo { get; set; }

        public List<IngredientViewModel> Ingredientes { get; set; }

        public List<List<IngredientViewModel>> IngredientesArticulosPromocion { get; set; }
    }
}
