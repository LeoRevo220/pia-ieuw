﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class PromotionViewModel
    {
        public int ID { get; set; }
        public string Nombre { get; set; }

        public float PrecioFinal { get; set; }

        public int ImagenID { get; set; }
        public List<ArticuloViewModel> Articulos { get; set; }

    }
}
