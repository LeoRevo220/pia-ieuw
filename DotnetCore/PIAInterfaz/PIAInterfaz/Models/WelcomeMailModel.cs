﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class WelcomeMailModel
    {
        public string ToEmail { get; set; }

        public string UserName { get; set; }
    }
}
