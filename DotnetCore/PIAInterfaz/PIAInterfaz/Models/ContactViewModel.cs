﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class ContactViewModel
    {
        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        [EmailAddress(ErrorMessage = "No es una direccion de correo valida")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        public string Asunto { get; set; }
        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        public string Mensaje { get; set; }
    }
}
