﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class PromocionArticuloCarritoModel
    {
        public int ID { get; set; }

        public int Cantidad { get; set; }

        public List<string> TiposDePanArticulos { get; set; }

        public List<List<int>> IngredientesArticulos { get; set; }
    }
}
