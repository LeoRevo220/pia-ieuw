﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class ArticuloCarritoModel
    {
        public int ID { get; set; }

        //public string Nombre { get; set; }
        //[Required]
        //[MinLength(length: 1)]
        public int Cantidad { get; set; }
        //[Required]
        public List<int> Ingredientes { get; set; }
        //[Required]
        public string  TipoDePan { get; set; }

    }
}
