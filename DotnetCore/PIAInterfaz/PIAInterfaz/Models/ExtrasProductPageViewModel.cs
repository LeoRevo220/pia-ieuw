﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class ExtrasProductPageViewModel
    {
        public ArticuloViewModel Articulo { get; set; }

        public List<ArticuloViewModel> ArticulosRelacionados { get; set; }
    }
}
