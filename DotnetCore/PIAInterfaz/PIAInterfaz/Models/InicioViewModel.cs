﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class InicioViewModel
    {
        public List<PizzaEspecialViewModel> PizzasEspeciales { get; set; }

        public List<ArticuloViewModel> MasVendidas { get; set; }

        public List<ArticuloViewModel> VuelveAOrdenar { get; set; }
    }
}
