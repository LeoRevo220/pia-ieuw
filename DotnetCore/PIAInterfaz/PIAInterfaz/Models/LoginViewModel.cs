﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "El campo de correo electronico es necesario")]
        [EmailAddress(ErrorMessage = "Esta no es una direccion de correo electronico valida")]
        public string Email { get; set; }

        [Required(ErrorMessage ="El campo  contraseña es necesario")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogin { get; set; }
    }
}
