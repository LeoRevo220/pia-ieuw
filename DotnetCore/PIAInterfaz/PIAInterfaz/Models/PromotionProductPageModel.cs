﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class PromotionProductPageModel
    {
        public PromotionViewModel Promotion { get; set; }

        public List<IngredientViewModel> Ingredientes { get; set; }
    }
}
