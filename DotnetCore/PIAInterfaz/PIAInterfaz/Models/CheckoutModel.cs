﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class CheckoutModel
    {
        [Required(ErrorMessage = "Es necesario introducir su nombre.")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Es necesario introducir su apellido.")]
        public string Apellido { get; set; }

        [Required(ErrorMessage = "Es necesario introducir su email.")]
        [EmailAddress(ErrorMessage ="Esta no es una direccion de correo electronica valida")]
        [Remote(action:"IsEmailInUse", controller:"MiCuenta")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Es necesario introducir su telefono")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Ingrese solamente numeros")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Es necesario introducir una direccion")]
        public string Direccion { get; set; }

        [Required(ErrorMessage = "Es necesario introducir una contraseña")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Es necesario introducir la misma contraseña que el campo anterior.")]
        [Compare("Password", ErrorMessage ="Las contraseñas no coinciden")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Por favor seleccione un metodo de pago.")]
        public string MetodoDePago { get; set; }

    }
}
