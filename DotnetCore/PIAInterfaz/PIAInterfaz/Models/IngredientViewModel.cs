﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class IngredientViewModel : IEquatable<IngredientViewModel>
    {
        public int ID { get; set; }

        public string Nombre { get; set; }

        public bool Equals(IngredientViewModel other)
        {
            if(other is null)
                return false;
            return this.ID == other.ID && this.Nombre == other.Nombre;
        }

       
    }
}
