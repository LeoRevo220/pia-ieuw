﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class PizzaEspecialViewModel
    {

        public int ID { get; set; }
        public float Precio { get; set; }

        public string Descripcion { get; set; }

        public string Nombre { get; set; }

        public int CantidadDeIngredientesMinimos { get; set; }

        public int ImagenID { get; set; }

        public List<IngredientViewModel> Ingredientes { get; set; }

    }
}
