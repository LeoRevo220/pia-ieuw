﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class OrderViewModel
    {
        public int CompraId { get; set; }

        public DateTime Fecha { get; set; }

        public float Total { get; set; }

        public string MetodoDePago { get; set; }

        public string Direccion { get; set; }

        public float CostoEnvio { get; set; }

        public List<ArticuloVendidoTempModel> Articulos { get; set; }
    }
}
