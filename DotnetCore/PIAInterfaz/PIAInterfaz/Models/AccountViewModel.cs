﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class AccountViewModel
    {
        [Required(ErrorMessage ="Por favor no deje vacio el campo nombre.")]
        public string Name { get; set; }

        [Required(ErrorMessage ="Por favor no deje vacio el campo apellido.")]
        public string LastName { get; set; }

        [RegularExpression("([0-9]+)", ErrorMessage = "Ingrese solamente numeros")]
        public string Phone { get; set; }

        public string Direccion { get; set; }
    }
}
