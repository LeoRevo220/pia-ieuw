﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class BasicPizzaProductViewModel
    {
        public PizzaBasicaViewModel PizzaInfo { get; set; }

        public List<IngredientViewModel> Ingredientes { get; set; }

        public List<PizzaBasicaViewModel> ArticulosRelacionados { get; set; }
    }
}
