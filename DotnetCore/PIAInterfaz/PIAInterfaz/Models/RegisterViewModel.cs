﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        [EmailAddress(ErrorMessage = "No es una direccion de correo valida")]
        [Remote(action:"IsEmailInUse", controller: "MiCuenta")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Es necesario introducir este campo")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Ingrese solamente numeros")]
        public string Telefono { get; set; }

        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Es necesario introducir este Campo")]
        [Compare("Password", ErrorMessage ="Las contraseñas no coinciden")]
        public string ConfirmPassword { get; set; }
    }
}
