﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public class CheckoutUserLoggedInModel
    {
        [Required(ErrorMessage = "Es necesario introducir una direccion")]
        public string Direccion { get; set; }
        [Required(ErrorMessage = "Es necesario introducir su telefono")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Ingrese solamente numeros")]
        public string Telefono { get; set; }

        [Required(ErrorMessage ="Por favor seleccione un metodo de pago.")]
        public string MetodoDePago { get; set; }

        
    }
}
