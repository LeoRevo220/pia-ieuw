﻿using PIAInterfaz.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Models
{
    public enum TipoDeArticulo
    {
        PIZZA,
        ESPECIAL,
        ESPAGUETI,
        EXTRA,
        COMPLEMENTO,
        BEBIDA,
        PROMOCION,
    }
    public class ArticuloViewModel
    {
    
        public int ID { get; set; }
        public float Precio { get; set; }

        public string Descripcion { get; set; }

        public string Nombre { get; set; }

        public int Ventas { get; set; }

        public int ImagenID { get; set; }

        public int? CantidadDeIngredientes { get; set; }

        public TipoDeArticulo Tipo { get; set; }

        public string TipoAction 
        { 
            get
            {
                switch (Tipo)
                {
                    case TipoDeArticulo.PIZZA:
                        return "Basics";
                    case TipoDeArticulo.ESPECIAL:
                        return "Specials";
                    case TipoDeArticulo.PROMOCION:
                        return "Offers";
                    default:
                        return "Extras";
                }
            }
        }
    }
}
