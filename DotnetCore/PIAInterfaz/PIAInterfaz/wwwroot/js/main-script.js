$("document").ready(function(){
    $('.menu-logo img').click(function(){
        window.location.href="/Inicio";
    });
    $(window).scroll(function () {
        menuFixed();
    });
    menuFixed();

    $('#backToTop').click(function(){
        $("html, body").stop().animate({scrollTop:0}, 500, 'swing');
        // alert("Hola que hace");
    });
    function menuFixed() {
       
        var height = $('header').height();
        var marginBottom = parseInt($('header').css('margin-bottom'));
        console.log(marginBottom);
        if ($(this).scrollTop() > 0) {
            $('header').addClass('fixed');
            $('main').css('padding-top', height+marginBottom);
        } else {
            $('header').removeClass('fixed');
            $('main').css('padding-top', 0);
        }
        

    }
});