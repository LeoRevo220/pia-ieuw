$('document').ready(function () {


    getCartElements();
    //addToCart();

   


})
function getCartElements() {
    //Vaciar los elementos del carrito
    var carritoRaiz = $('.car-popover-content');
    var articuloTotal = $('<p></p>').addClass('car-popover-total').text('No hay elementos en el carrito');

    carritoRaiz.empty();
    carritoRaiz.append(articuloTotal);


    $.get("/Carrito/GetAllArticulosEnCarrito", function (data) {

        var jsonObject = JSON.parse(data);

        var precioTotal = 0;
        $('#mobileCart').attr('data-count', jsonObject.length);

        $.each(jsonObject, function (key, value) {
            var articuloEnCarrito = $('<div></div>').addClass("car-popover-item");

            var articuloImagen = $('<img>').attr("src", "/Imagenes/GetImage?id=" + value.ImagenID);

            var articuloContenido = $('<div></div>').addClass("car-popover-item-info");

            var productPath = "";
            switch (value.TipoDeArticulo) {
                case "Extras":
                    productPath = "Extras";
                    break;
                case "Basics":
                    productPath = "PizzaBasica";
                    break;
                case "Specials":
                    productPath = "Especiales";
                    break;
                case "Offers":
                    productPath = "Promocion";
                    break;
            }

            var articuloTitulo = $('<p></p>').addClass("car-popover-item-title").append($('<a></a>').attr('href', '/Producto/' + productPath + '?id=' + value.ArticuloVendidoID).text(value.Nombre));

            var articuloCantidad = $('<p></p>').addClass("car-popover-item-label").text("Cantidad: " + value.Cantidad);
            var articuloTipoPan = null;
            if (value.TipoDePan != null) {
                articuloTipoPan = $('<p></p>').addClass("car-popover-item-label").html("Tipo de Pan: <b>" + value.TipoDePan +'</b>');
            }
            var articuloSubtotal = $('<p></p>').addClass("car-popover-item-money").text("Subtotal: $" + value.Subtotal);

            articuloContenido.append(articuloTitulo, articuloCantidad);
            if (articuloTipoPan != null) {
                articuloContenido.append(articuloTipoPan);
            }

            articuloContenido.append(articuloSubtotal);
            articuloEnCarrito.append(articuloImagen, articuloContenido);

            carritoRaiz.append(articuloEnCarrito);
            precioTotal += value.Subtotal;
        });

        if (jsonObject.length != 0) {
            $('.car-popover-total').remove();

            //var articuloTotal = $('<p></p>').addClass('car-popover-total').text('Total: $' + precioTotal);
            articuloTotal.text('Total: $' + precioTotal);
            carritoRaiz.append(articuloTotal);
        }
    });

}

function showSuccessMessage() {
    $('.car-popover-content').addClass('show');
    $('.car-popover-content').append($('<p></p>').addClass('mensajeExitoCarrito').text('Articulo(s) agregado al carrito'));
    setTimeout(function () {
        $('.car-popover-content').removeClass('show');
        
    }, 2000, function () { $('.mensajeExitoCarrito').remove(); });
}
