﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class Mensaje
    {
        public int ID { get; set; }


        public string Nombre { get; set; }

        public string Email { get; set; }

        public string Asunto { get; set; }

        public string ContenidoMensaje { get; set; }

    }
}
