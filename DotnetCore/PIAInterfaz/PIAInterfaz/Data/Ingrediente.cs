﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class Ingrediente
    {
        public int ID { get; set; }


        public string Nombre { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }

        public ICollection<ArticuloVendido> ArticuloVendidos { get; set; }
    }
}
