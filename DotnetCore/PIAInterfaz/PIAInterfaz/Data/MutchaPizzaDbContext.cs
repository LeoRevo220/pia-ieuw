﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class MutchaPizzaDbContext : IdentityDbContext<Usuario>
    {
        public MutchaPizzaDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Articulo> Articulos { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }

        public DbSet<PizzaEspecial> PizzasEspeciales {get; set;}

        public DbSet<Ingrediente> Ingredientes { get; set; }

        public DbSet<Promocion> Promociones { get; set; }

        public DbSet<Espagueti> Espaguetis { get; set; }

        public DbSet<Complemento> Complementos { get; set; }

        public DbSet<Bebida> Bebidas { get; set; }

        public DbSet<ArticuloVendido> ArticulosVendidos { get; set; }

        public DbSet<Imagen> Imagenes { get; set; }

        public DbSet<Compra> Compras { get; set; }

        public DbSet<Mensaje> Mensajes { get; set; }
    }
}
