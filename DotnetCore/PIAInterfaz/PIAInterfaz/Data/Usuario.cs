﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class Usuario : IdentityUser
    {
        public string Nombres { get; set; }

        public string Apellidos { get; set; }
        public string Direccion { get; set; }

        public string NumeroDeTelefono { get; set; }

        public ICollection<Compra> Compras { get; set; }
    }
}
