﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    
    public class Articulo
    {
        public int ID { get; set; }
        public float Precio { get; set; }

        public string Descripcion { get; set; }

        public string Nombre { get; set; }

        public virtual ICollection<ArticuloVendido> ArticulosVendidos { get; set; }
        public int? ImagenID { get; set; }

        public Imagen Imagen { get; set; }

        public bool Activo { get; set; }
    }
}
