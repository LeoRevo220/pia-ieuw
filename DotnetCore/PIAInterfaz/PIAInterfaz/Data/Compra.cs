﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class Compra
    {
        public int ID { get; set; }

        public string UsuarioId { get; set; }

        public Usuario Usuario{ get; set; }

        public DateTime? Fecha { get; set; }

        public float PrecioTotal { get; set; }

        public string MetodoDePago { get; set; }

        public string Direccion { get; set; }

        public virtual ICollection<ArticuloVendido> ArticulosVendidos { get; set; }

    }
}
