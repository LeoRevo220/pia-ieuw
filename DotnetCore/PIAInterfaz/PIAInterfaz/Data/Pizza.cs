﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class Pizza : Articulo
    {
        public int CantidadDeIngredientesMinimos { get; set; }
        public string TipoDePan { get; set; }

        public virtual ICollection<Ingrediente> Ingredientes { get; set; }
    }
}
