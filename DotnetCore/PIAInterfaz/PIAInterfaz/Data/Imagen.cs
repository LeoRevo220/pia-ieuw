﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class Imagen
    {
        [Key]
        public int ImagenID { get; set; }

        public byte[] ImagenData { get; set; }


        public virtual IEnumerable<Articulo> Articulos { get; set; }
    }
}
