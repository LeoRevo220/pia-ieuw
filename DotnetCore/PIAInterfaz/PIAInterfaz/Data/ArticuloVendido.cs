﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Data
{
    public class ArticuloVendido
    {
        [Key]
        public int ArticuloVendidoID { get; set; }

        public float Precio { get; set; }

        public string Nombre { get; set; }
        public string TipoDePan { get; set; }

        public int Cantidad { get; set; }
        public int ArticuloID { get; set; }

        public Articulo Articulo { get; set; }

        public int CompraID { get; set; }
        public Compra Compra { get; set; }

        public virtual ICollection<Ingrediente> Ingredientes { get; set; }
    }
}
