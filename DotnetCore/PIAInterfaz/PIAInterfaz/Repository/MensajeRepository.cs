﻿using PIAInterfaz.Data;
using PIAInterfaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Repository
{
    public class MensajeRepository
    {
        private readonly MutchaPizzaDbContext context;
        public MensajeRepository(MutchaPizzaDbContext context)
        {
            this.context = context;
        }

        public async Task<int> AddMessage(ContactViewModel mensaje)
        {
            var mensajeToDb = new Mensaje()
            {
                Nombre = mensaje.Nombre,
                Email = mensaje.Email,
                Asunto = mensaje.Asunto,
                ContenidoMensaje = mensaje.Mensaje
            };

            await context.Mensajes.AddAsync(mensajeToDb);
            await context.SaveChangesAsync();
            return mensajeToDb.ID;
        }
    }
}
