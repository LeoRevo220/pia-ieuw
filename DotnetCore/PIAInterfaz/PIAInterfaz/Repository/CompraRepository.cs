﻿using Microsoft.EntityFrameworkCore;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Repository
{
    public class CompraRepository
    {
        private readonly MutchaPizzaDbContext context;
        public CompraRepository(MutchaPizzaDbContext context)
        {
            this.context = context;
        }

        public async Task<int> AddOrder(Compra compra)
        {
            await context.Compras.AddAsync(compra);
            await context.SaveChangesAsync();

            return compra.ID;
        }

        public async Task<OrderViewModel> GetOrderById(int id)
        {
            var order = await context.Compras.Where(o => o.ID == id).Select(o => new OrderViewModel()
            {
                CompraId = id,
                CostoEnvio = 30,
                Direccion = o.Direccion,
                Fecha = o.Fecha ?? DateTime.Now,
                Total = o.PrecioTotal,
                MetodoDePago = o.MetodoDePago,
                Articulos = o.ArticulosVendidos.Select(articulo=> new ArticuloVendidoTempModel() {
                    Nombre = articulo.Nombre, 
                    Cantidad = articulo.Cantidad,
                    Precio = articulo.Precio,
                    Subtotal = articulo.Cantidad * articulo.Precio,
                    TipoDePan = articulo.TipoDePan,
                    Ingredientes = articulo.Ingredientes.Count > 0 ? articulo.Ingredientes.Select(ingrediente=> new IngredientViewModel() { ID = ingrediente.ID, Nombre = ingrediente.Nombre}).ToList() : null,
                }).ToList()
            }).FirstOrDefaultAsync();


            return order;
        }
    }
}
