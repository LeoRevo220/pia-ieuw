﻿using Microsoft.EntityFrameworkCore;
using PIAInterfaz.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Repository
{
    public class ImagenRepository
    {
        private readonly MutchaPizzaDbContext context;

        public ImagenRepository(MutchaPizzaDbContext context)
        {
            this.context = context;
        }

        public async Task<byte[]> getImageById(int id)
        {
            var imagen = await context.Imagenes.Where(i => i.ImagenID == id).FirstOrDefaultAsync();

            return imagen.ImagenData;
        }
    }
}
