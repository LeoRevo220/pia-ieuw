﻿using Microsoft.EntityFrameworkCore;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Repository
{
    public class ArticuloRepository
    {
        private readonly MutchaPizzaDbContext context;
        public ArticuloRepository(MutchaPizzaDbContext context)
        {
            this.context = context;
        }

        public async Task<List<PizzaEspecialViewModel>> getAllSpecialPizzas()
        {
            var list = await context.PizzasEspeciales.Select(p => new PizzaEspecialViewModel()
            {
                ID = p.ID,
                Nombre = p.Nombre,
                Descripcion = p.Descripcion,
                Precio = p.Precio,
                ImagenID = p.ImagenID ?? 0,
                Ingredientes = p.Ingredientes.Select(i => new IngredientViewModel() { ID=i.ID, Nombre = i.Nombre}).ToList()
            }).ToListAsync();


            return list;
        }

        public async Task<PizzaEspecialViewModel> getSpecialPizza(int id)
        {
            var pizza = await context.PizzasEspeciales.Where(p =>p.ID == id).Select(p => new PizzaEspecialViewModel()
            {
                ID = p.ID,
                Nombre = p.Nombre,
                Descripcion = p.Descripcion,
                Precio = p.Precio,
                ImagenID = p.ImagenID ?? 0,
                Ingredientes = p.Ingredientes.Select(i=> new IngredientViewModel() {ID=i.ID, Nombre = i.Nombre }).ToList()
            }).FirstOrDefaultAsync();

            return pizza;
        }

        public async Task<List<PizzaBasicaViewModel>> getAllBasicPizzas()
        {
            var basicPizzas = await context.Pizzas.Where(p => !(p is PizzaEspecial) && p.Activo).Select(p => new PizzaBasicaViewModel()
            {
                ID = p.ID,
                Nombre = p.Nombre,
                Descripcion = p.Descripcion,
                CantidadDeIngredientesMinimos = p.CantidadDeIngredientesMinimos,
                Precio = p.Precio,
                ImagenID = p.ImagenID ?? 0
                
            }).ToListAsync();
            return basicPizzas;
        }

        public async Task<PizzaBasicaViewModel> getPizzaBasica(int id)
        {
            var pizzaBasica = await context.Pizzas.Where(p => !(p is PizzaEspecial) && p.ID == id).Select(p => new PizzaBasicaViewModel()
            {
                ID = p.ID,
                Nombre = p.Nombre,
                Descripcion = p.Descripcion,
                CantidadDeIngredientesMinimos = p.CantidadDeIngredientesMinimos,
                Precio = p.Precio,
                ImagenID = p.ImagenID ?? 0
            }).FirstOrDefaultAsync();


            return pizzaBasica;
        }

        public async Task<List<ArticuloViewModel>> getAllSpaghettis()
        {
            var listOfSpaghettis = await context.Espaguetis.Select(e => new ArticuloViewModel()
            {
                ID = e.ID,
                Nombre = e.Nombre,
                Descripcion = e.Descripcion,
                Precio = e.Precio,
                ImagenID = e.ImagenID ?? 0
            }).ToListAsync();

            return listOfSpaghettis;
        }

        public async Task<List<ArticuloViewModel>> getAllComplements()
        {
            var listOfComplements = await context.Complementos.Select(c => new ArticuloViewModel()
            {
                ID = c.ID,
                Nombre = c.Nombre,
                Descripcion = c.Descripcion,
                Precio = c.Precio,
                ImagenID = c.ImagenID ?? 0
            }).ToListAsync();

            return listOfComplements;
        }

        public async Task<List<ArticuloViewModel>> getAllDrinks()
        {
            var listOfDrinks = await context.Bebidas.Select(b => new ArticuloViewModel()
            {
                ID = b.ID,
                Nombre = b.Nombre,
                Descripcion = b.Descripcion,
                Precio = b.Precio,
                ImagenID = b.ImagenID ?? 0
            }).ToListAsync();


            return listOfDrinks;
        }



        public async Task<List<ArticuloViewModel>> getTopSellers()
        {
            
            var listOfTopSellers = await context.Articulos.Where(a => a.ArticulosVendidos.Count() > 0).Select(q => new ArticuloViewModel()
            {
                ID = q.ID,
                Nombre = q.Nombre,
                Descripcion = q.Descripcion,
                Precio = q.Precio,
                Ventas = q.ArticulosVendidos.Sum(articulo=>articulo.Cantidad),
                ImagenID = q.ImagenID ?? 0,
                Tipo = q is PizzaEspecial ? TipoDeArticulo.ESPECIAL : q is Pizza ? TipoDeArticulo.PIZZA : q is Promocion ? TipoDeArticulo.PROMOCION : TipoDeArticulo.EXTRA

            }).OrderByDescending(x => x.Ventas).Take(8).ToListAsync();


            return listOfTopSellers;
        }


        public async Task<List<ArticuloViewModel>> getRecentArticlesByUser(string userID)
        {
            var listOfRecentArticles = await context.Articulos.Where(articulo => articulo.ArticulosVendidos.Any(artVendido=>artVendido.Compra.UsuarioId == userID))
                .OrderByDescending(a => a.ArticulosVendidos.OrderByDescending(articuloVendido => articuloVendido.Compra.Fecha).FirstOrDefault())
                .Select(a => new ArticuloViewModel()
                {
                    ID = a.ID,
                    Nombre = a.Nombre,
                    Descripcion = a.Descripcion,
                    Precio = a.Precio,
                    ImagenID = a.ImagenID ?? 0,
                    Tipo = a is PizzaEspecial ? TipoDeArticulo.ESPECIAL : a is Pizza ? TipoDeArticulo.PIZZA : a is Promocion ? TipoDeArticulo.PROMOCION : TipoDeArticulo.EXTRA
                }).Take(8).ToListAsync();

            return listOfRecentArticles;
        }

        public async Task<ArticuloViewModel> getGenericArticleById(int id)
        {
            var genericProduct = await context.Articulos.Where(a => a.ID == id).Select(a => new ArticuloViewModel()
            {
                ID = a.ID,
                Nombre = a.Nombre,
                Precio = a.Precio,
                Descripcion = a.Descripcion,
                ImagenID = a.ImagenID ?? 0,
                Tipo = a is Bebida ?  TipoDeArticulo.BEBIDA : a is Complemento ? TipoDeArticulo.COMPLEMENTO : a is Espagueti ? TipoDeArticulo.ESPAGUETI : TipoDeArticulo.ESPAGUETI
            }).FirstOrDefaultAsync();


            return genericProduct;
        }


        public async Task<PromotionViewModel> getPizzaPromotionById(int id)
        {
            var promotion = await context.Promociones.Where(p => p.ID == id).Select(p => new PromotionViewModel()
            {
                ID = p.ID,
                Nombre = p.Nombre,
                PrecioFinal = p.Precio,
                ImagenID = p.ImagenID ?? 0,
                Articulos = p.Articulos.Select(articulosPromocion => new ArticuloViewModel() { 
                    ImagenID = articulosPromocion.ImagenID ?? 0,
                    Nombre = articulosPromocion.Nombre,
                    Descripcion = articulosPromocion.Descripcion,
                    CantidadDeIngredientes = ((Pizza)articulosPromocion).CantidadDeIngredientesMinimos
                }).ToList()

            }).FirstOrDefaultAsync();

            return promotion;
        }

        public async Task<float> getProductPrice(int id)
        {
            var price = await context.Articulos.Where(a => a.ID == id).Select(p => p.Precio).FirstOrDefaultAsync();

            return price;
        }

        public async Task<int> AddItemsToOrder(List<ArticuloVendidoTempModel> articulos, int compraId)
        {

            var articulosVendidos = articulos.Select(articulo => new ArticuloVendido()
            {
                Precio = articulo.Precio,
                Nombre = articulo.Nombre,
                Cantidad = articulo.Cantidad,
                ArticuloID = articulo.ArticuloVendidoID,
                TipoDePan = articulo.TipoDePan,
                CompraID = compraId,
                Ingredientes = context.Ingredientes.AsEnumerable().Where(i=>articulo.Ingredientes != null && articulo.Ingredientes.Any(a=>a.ID==i.ID)).ToList()
            }).ToList();



            await context.ArticulosVendidos.AddRangeAsync(articulosVendidos);
            var result = await context.SaveChangesAsync();
            //context.Ingredientes.Atta

            return result;
        }

        public async Task<bool> IsOfferValid(int id, string userId)
        {
            var result = await context.Compras.AnyAsync(c => c.UsuarioId == userId && c.ArticulosVendidos.Any(av => av.ArticuloID == id) && c.Fecha.Value.Date == DateTime.Now.Date);

            return !result;
        }
    }
}
