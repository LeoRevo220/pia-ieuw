﻿using Microsoft.EntityFrameworkCore;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Repository
{
    public class IngredienteRepository
    {
        private readonly MutchaPizzaDbContext context;

        public IngredienteRepository(MutchaPizzaDbContext context)
        {
            this.context = context;
        }

        public async Task<List<IngredientViewModel>> getAllIngredients()
        {
            var listOfIngredients = await context.Ingredientes.Select(i => new IngredientViewModel()
            {
                    ID = i.ID,
                    Nombre = i.Nombre
            }).ToListAsync();

            return listOfIngredients;
        }

        public async Task<List<IngredientViewModel>> getIngredientsNameForCollection(List<int> ingredients)
        {
            var listOfIngredients = await context.Ingredientes.Where(i => ingredients.Any(ingredienteID => ingredienteID == i.ID)).Select(i=>new IngredientViewModel()
            {
                ID = i.ID,
                Nombre = i.Nombre
            }).ToListAsync();

            return listOfIngredients;
        }
    }
}
