﻿using PIAInterfaz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequestModel mailRequest);

        Task SendWelcomeEmailAsync(WelcomeMailModel mailRequest, string path);
    }
}
