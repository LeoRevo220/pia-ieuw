﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PIAInterfaz.Models;
using PIAInterfaz.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Controllers
{
    public class ProductoController : Controller
    {
        private readonly ArticuloRepository articuloRepository;
        private readonly IngredienteRepository ingredienteRepository;
        public ProductoController(ArticuloRepository articuloRepository, IngredienteRepository ingredienteRepository)
        {
            this.articuloRepository = articuloRepository;
            this.ingredienteRepository = ingredienteRepository;
        }

        private bool ShowOnboarding(string name)
        {
            bool showOnboarding = true;
            if (HttpContext.Session.GetString(name) != null)
                showOnboarding = false;
            else
                HttpContext.Session.SetString(name, "Visited");
            return showOnboarding;
        }

        [Route("/Producto/Especiales")]
        public async Task<IActionResult> Specials(int id = 0)
        {
            var pizzaEspecial = await articuloRepository.getSpecialPizza(id);

            var pizzasEspeciales = await articuloRepository.getAllSpecialPizzas();


            var elementoRepetido = pizzasEspeciales.SingleOrDefault(p => p.ID == id);
            pizzasEspeciales.Remove(elementoRepetido);

            var model = new SpecialPizzaProductViewModel()
            {
                Pizza = pizzaEspecial,
                ArticulosRelacionados = pizzasEspeciales
            };
            ViewBag.ShowOnboarding = ShowOnboarding("Product-Specials");
            return View("SpecialsProductPage", model);
        }

        [Route("/Producto/PizzaBasica")]
        public async Task<IActionResult> Basics(int id = 0)
        {
            var pizzaBasica = await articuloRepository.getPizzaBasica(id);
            var ingredientes = await ingredienteRepository.getAllIngredients();

            var articulosRelacionados = await articuloRepository.getAllBasicPizzas();

            var elementoRepetido = articulosRelacionados.SingleOrDefault(a => a.ID == id);
            articulosRelacionados.Remove(elementoRepetido);

            var model = new BasicPizzaProductViewModel()
            {
                PizzaInfo = pizzaBasica,
                Ingredientes = ingredientes,
                ArticulosRelacionados = articulosRelacionados
            };
            ViewBag.ShowOnboarding = ShowOnboarding("Product-Basics");
            return View("BasicsProductPage", model);
        }

        public async Task<IActionResult> Extras(int id = 0)
        {
            var product = await articuloRepository.getGenericArticleById(id);

            List<ArticuloViewModel> articulosRelacionados = null;
            switch (product.Tipo)
            {
                case TipoDeArticulo.BEBIDA:
                    articulosRelacionados = await articuloRepository.getAllDrinks();
                    break;
                case TipoDeArticulo.ESPAGUETI:
                    articulosRelacionados = await articuloRepository.getAllSpaghettis();
                    break;
                case TipoDeArticulo.COMPLEMENTO:
                    articulosRelacionados = await articuloRepository.getAllComplements();
                    break;
            }

            var articuloRepetido = articulosRelacionados.SingleOrDefault(a => a.ID == id);
            articulosRelacionados.Remove(articuloRepetido);
            var model = new ExtrasProductPageViewModel()
            {
                Articulo = product,
                ArticulosRelacionados = articulosRelacionados
            };
            ViewBag.ShowOnboarding = ShowOnboarding("Product-Extras");
            return View("ExtrasProductPage", model);
        }
        [Route("/Producto/Promocion")]
        public async Task<IActionResult> Offers(int id = 0)
        {
            var offer = await articuloRepository.getPizzaPromotionById(id);
            var ingredientes = await ingredienteRepository.getAllIngredients();
            var model = new PromotionProductPageModel()
            {
                Promotion = offer,
                Ingredientes = ingredientes
            };

            ViewBag.ShowOnboarding = ShowOnboarding("Product-Offers");
            return View("PromotionsProductPage", model);
        }


    }
}
