﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using PIAInterfaz.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace PIAInterfaz.Controllers
{
    public class CarritoController : Controller
    {
        private readonly ArticuloRepository articuloRepository;
        private readonly CompraRepository compraRepository;
        private readonly IngredienteRepository ingredienteRepository;
        private readonly SignInManager<Usuario> signInManager; 
        private readonly UserManager<Usuario> userManager; 
        public CarritoController(ArticuloRepository articuloRepository, IngredienteRepository ingredienteRepository, CompraRepository compraRepository, SignInManager<Usuario> signInManager, UserManager<Usuario> userManager)
        {
            this.articuloRepository = articuloRepository;
            this.ingredienteRepository = ingredienteRepository;
            this.compraRepository = compraRepository;
            this.signInManager = signInManager;
            this.userManager = userManager;
        }
        private bool ShowOnboarding(string name)
        {
            bool showOnboarding = true;
            if (HttpContext.Session.GetString(name) != null)
                showOnboarding = false;
            else
                HttpContext.Session.SetString(name, "Visited");
            return showOnboarding;
        }

        [Route("/Ordenes")]
        public async Task<IActionResult> GetOrder(int id = 0)
        {
            var model = await compraRepository.GetOrderById(id);
            return View("CompraRealizada", model);
        }

        public IActionResult Index()
        {
            var articulosVendidos = getCartListSession();

            //Traerse el link del articulo
            ViewBag.ShowOnboarding = ShowOnboarding("Carrito-Index");
            return View("Carrito", articulosVendidos);
        }

        public IActionResult Checkout()
        {
            var articulosVendidos = getCartListSession();
            if(articulosVendidos.Count > 0)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("CheckoutUser");
                }
                ViewBag.Subtotal = articulosVendidos.Sum(a => a.Subtotal);
                ViewBag.Envio = 30;
                ViewBag.Total = ViewBag.Subtotal + ViewBag.Envio;
                ViewBag.ShowOnboarding = ShowOnboarding("Carrito-Checkout");
                return View();
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> CheckoutUser()
        {
            if (!User.Identity.IsAuthenticated)
            {
                //TODO: Mandar a pagina de error
                return RedirectToAction("Login", "MiCuenta",new { returnUrl ="/Carrito/Checkout"});
            }
            var userLoggedIn = await userManager.GetUserAsync(User);

            var model = new CheckoutUserLoggedInModel()
            {
                Telefono = userLoggedIn.NumeroDeTelefono,
                Direccion = userLoggedIn.Direccion
            };

            var articulosVendidos = getCartListSession();
            ViewBag.Subtotal = articulosVendidos.Sum(a => a.Subtotal);
            ViewBag.Envio = 30;
            ViewBag.Total = ViewBag.Subtotal + ViewBag.Envio;
            ViewBag.ShowOnboarding = ShowOnboarding("Carrito-CheckoutUser");
            return View("CheckoutLoggedIn", model);
        }

        [HttpPost]
        public async Task<IActionResult> Checkout(CheckoutModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {


                    var newUser = new Usuario()
                    {
                        Nombres = model.Nombre,
                        Apellidos = model.Apellido,
                        Email = model.Email,
                        UserName = model.Email
                    };

                    var result = await userManager.CreateAsync(newUser, model.Password);

                    if (result.Succeeded)
                    {
                        await signInManager.SignInAsync(newUser, isPersistent: false);

                        var userRegistered = await userManager.FindByEmailAsync(model.Email);
                        string userID = userRegistered.Id;
                        var articulosEnCarrito = getCartListSession();
                        var precioTotal = articulosEnCarrito.Sum(a => a.Subtotal);
                        var compra = new Compra()
                        {

                            Fecha = DateTime.Now,
                            UsuarioId = userID,
                            PrecioTotal = precioTotal +30,
                            MetodoDePago = model.MetodoDePago,
                            Direccion = model.Direccion
                        };

                        var compraID = await compraRepository.AddOrder(compra);
                        if (compraID != 0)
                        {
                            var resultOrder = await articuloRepository.AddItemsToOrder(articulosEnCarrito, compraID);
                            HttpContext.Session.Remove("ArticulosVendidos");
                            var orderModel = new OrderViewModel()
                            {
                                CompraId = compraID,
                                Articulos = articulosEnCarrito,
                                CostoEnvio = 30,
                                Fecha = compra.Fecha ?? DateTime.Now,
                                Direccion = compra.Direccion,
                                MetodoDePago = compra.MetodoDePago,
                                Total = compra.PrecioTotal
                            };

                            return View("CompraRealizada", orderModel);
                        }
                    }
                    else
                    {
                        //TODO: No se puede registar un usuario que ya existe
                        return View("Error");
                    }

                }

            } else
            {
                return RedirectToAction("CheckoutUser");
            }

            var articulosVendidos = getCartListSession();
            ViewBag.Subtotal = articulosVendidos.Sum(a => a.Subtotal);
            ViewBag.Envio = 30;
            ViewBag.Total = ViewBag.Subtotal + ViewBag.Envio;
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> CheckoutUser(CheckoutUserLoggedInModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    var user = await userManager.GetUserAsync(User);

                    var articulosEnCarrito = getCartListSession();
                    if(articulosEnCarrito.Count > 0)
                    {
                        var precioTotal = articulosEnCarrito.Sum(a => a.Subtotal);
                        var compra = new Compra()
                        {

                            Fecha = DateTime.Now,
                            UsuarioId = user.Id,
                            PrecioTotal = precioTotal +30,
                            MetodoDePago = model.MetodoDePago,
                            Direccion = model.Direccion
                        };

                        var compraID = await compraRepository.AddOrder(compra);
                        if (compraID != 0)
                        {
                            var resultOrder = await articuloRepository.AddItemsToOrder(articulosEnCarrito, compraID);

                            user.Direccion = model.Direccion;
                            user.NumeroDeTelefono = model.Telefono;
                            await userManager.UpdateAsync(user);

                            HttpContext.Session.Remove("ArticulosVendidos");
                            var orderModel = new OrderViewModel()
                            {
                                CompraId = compraID,
                                Articulos = articulosEnCarrito,
                                CostoEnvio = 30,
                                Fecha = compra.Fecha ?? DateTime.Now,
                                Direccion = compra.Direccion,
                                MetodoDePago = compra.MetodoDePago,
                                Total = compra.PrecioTotal
                            };
                            return View("CompraRealizada", orderModel);
                        }

                    } else
                    {
                        return RedirectToAction("Index");
                    }
                }
            }

            //Mandar el viewbag otra vez
            var articulosVendidos = getCartListSession();
            ViewBag.Subtotal = articulosVendidos.Sum(a => a.Subtotal);
            ViewBag.Envio = 30;
            ViewBag.Total = ViewBag.Subtotal + ViewBag.Envio;
            return View("CheckoutLoggedIn");
        }

        [HttpPost]
        public async Task<IActionResult> AgregarArticuloPizzaBasica(ArticuloCarritoModel data)
        {
            var article = await articuloRepository.getPizzaBasica(data.ID);
            Dictionary<string, bool> validations = new Dictionary<string, bool>();
            validations["Cantidad"] = true;
            if (data.Cantidad <= 0)
                validations["Cantidad"] = false;
            
            validations["TipoDePan"] = true;
            if (data.TipoDePan == null)
                validations["TipoDePan"] = false;
            
            validations["Ingredientes"] = true;
            if (data.Ingredientes.Count < article.CantidadDeIngredientesMinimos)
                validations["Ingredientes"] = false;

            if (validations["Cantidad"] && validations["TipoDePan"] && validations["Ingredientes"])
            {

                var ingredientsWithName = await ingredienteRepository.getIngredientsNameForCollection(data.Ingredientes);

                var articulosVendidos = getCartListSession();

                var elementoRepetido = articulosVendidos
                    .Where(a => a.ArticuloVendidoID == data.ID && a.TipoDePan == data.TipoDePan && a.Ingredientes.SequenceEqual(ingredientsWithName))
                    .FirstOrDefault();
                if(elementoRepetido != null)
                {
                    elementoRepetido.Cantidad += data.Cantidad;
                    elementoRepetido.Subtotal = elementoRepetido.Cantidad * article.Precio;

                } else
                {
                    articulosVendidos.Insert(0,new ArticuloVendidoTempModel()
                    {
                        ArticuloEnCarritoID = Guid.NewGuid(),
                        Nombre = article.Nombre,
                        ArticuloVendidoID = article.ID,
                        Cantidad = data.Cantidad,
                        Precio = article.Precio,
                        Subtotal = data.Cantidad*article.Precio +(30*(ingredientsWithName.Count - article.CantidadDeIngredientesMinimos)),
                        TipoDePan = data.TipoDePan,
                        TipoDeArticulo = "Basics",
                        Ingredientes = ingredientsWithName,
                        ImagenID = article.ImagenID
                    });

                }

                var articlesOnCartSerielized = JsonConvert.SerializeObject(articulosVendidos);
                HttpContext.Session.SetString("ArticulosVendidos", articlesOnCartSerielized);


                return Json(true);
            }

            return Json(false);
        }


        [HttpPost]
        public async Task<IActionResult> AgregarArticuloPizzaEspecial(ArticuloCarritoModel data)
        {
            var article = await articuloRepository.getSpecialPizza(data.ID);
            Dictionary<string, bool> validations = new Dictionary<string, bool>();
            validations["Cantidad"] = true;
            if (data.Cantidad <= 0)
                validations["Cantidad"] = false;

            validations["TipoDePan"] = true;
            if (data.TipoDePan == null)
                validations["TipoDePan"] = false;

            if(validations["Cantidad"] && validations["TipoDePan"])
            {
                var articulosVendidos = getCartListSession();

                var elementoRepetido = articulosVendidos.Where(a => a.ArticuloVendidoID == article.ID && a.TipoDePan == data.TipoDePan).FirstOrDefault();
                if (elementoRepetido != null)
                {
                    elementoRepetido.Cantidad += data.Cantidad;
                    elementoRepetido.Subtotal = elementoRepetido.Cantidad * article.Precio;
                }
                else
                {
                    articulosVendidos.Insert(0, new ArticuloVendidoTempModel()
                    {
                        ArticuloEnCarritoID = Guid.NewGuid(),
                        Nombre = article.Nombre,
                        ArticuloVendidoID = article.ID,
                        Cantidad = data.Cantidad,
                        Precio = article.Precio,
                        Subtotal = data.Cantidad * article.Precio,
                        TipoDePan = data.TipoDePan,
                        TipoDeArticulo = "Specials",
                        ImagenID = article.ImagenID,
                        Ingredientes = article.Ingredientes
                    });

                }

          


                var articlesOnCartSerielized = JsonConvert.SerializeObject(articulosVendidos);
                HttpContext.Session.SetString("ArticulosVendidos", articlesOnCartSerielized);



                return Json(true);

            }

            return Json(false);
        }

        [HttpPost]
        public async Task<IActionResult> AgregarArticuloExtra(ArticuloCarritoModel data)
        {
            var article = await articuloRepository.getGenericArticleById(data.ID);
            Dictionary<string, bool> validations = new Dictionary<string, bool>();
            validations["Cantidad"] = true;
            if (data.Cantidad <= 0)
                validations["Cantidad"] = false;

            if (validations["Cantidad"])
            {
                var articulosVendidos = getCartListSession();

                var elementoRepetido = articulosVendidos.Where(a => a.ArticuloVendidoID == article.ID).FirstOrDefault();

                if (elementoRepetido != null)
                {
                    elementoRepetido.Cantidad += data.Cantidad;
                    elementoRepetido.Subtotal = elementoRepetido.Cantidad * article.Precio;
                }
                else
                {
                    articulosVendidos.Insert(0, new ArticuloVendidoTempModel()
                    {
                        ArticuloEnCarritoID = Guid.NewGuid(),
                        Nombre = article.Nombre,
                        ArticuloVendidoID = article.ID,
                        Cantidad = data.Cantidad,
                        Precio = article.Precio,
                        TipoDeArticulo = "Extras",
                        Subtotal = data.Cantidad * article.Precio,
                        ImagenID = article.ImagenID
                    });

                }


                var articlesOnCartSerielized = JsonConvert.SerializeObject(articulosVendidos);
                HttpContext.Session.SetString("ArticulosVendidos", articlesOnCartSerielized);

                return Json(true);
            }
            return Json(false);
        }

        public async Task<JsonResult> AgregarArticuloPromocion(PromocionArticuloCarritoModel data)
        {
            var article = await articuloRepository.getPizzaPromotionById(data.ID);

            Dictionary<string, bool> validations = new Dictionary<string, bool>();
            validations["TiposDePanes"] = true;
            if (data.TiposDePanArticulos.Any(tiposDePan => tiposDePan == null))
                validations["TiposDePanes"] = false;

            validations["Ingredientes"] = true;
            if(data.IngredientesArticulos.Any(listasIngredientes => listasIngredientes.Count < article.Articulos.FirstOrDefault().CantidadDeIngredientes))
                validations["Ingredientes"] = false;

            if(validations["TiposDePanes"] && validations["Ingredientes"])
            {
                var ingredientesPorArticulo = new List<List<IngredientViewModel>>();
                foreach (var lista in data.IngredientesArticulos)
                {
                    var ingredientsWithName = await ingredienteRepository.getIngredientsNameForCollection(lista);
                    ingredientesPorArticulo.Add(ingredientsWithName);
                }


                var articulosVendidos = getCartListSession();
                var elementoRepetido = articulosVendidos.SingleOrDefault(a => a.ArticuloVendidoID == data.ID);


                int resultado = 1;
                if (elementoRepetido == null)
                {
                    //TODO: Revisar si el dia actual compro una pizza
                    if (User.Identity.IsAuthenticated)
                    {
                        var userId = userManager.GetUserId(User);
                        if (!(await articuloRepository.IsOfferValid(data.ID, userId)))
                        {
                            resultado = 2;
                        }

                    }
                    if(resultado != 2)
                    {
                        articulosVendidos.Insert(0, new ArticuloVendidoTempModel()
                        {
                            ArticuloEnCarritoID = Guid.NewGuid(),
                            Nombre = article.Nombre,
                            ArticuloVendidoID = article.ID,
                            Precio = article.PrecioFinal,
                            Subtotal = article.PrecioFinal,
                            TipoDeArticulo = "Offers",
                            ImagenID = article.ImagenID,
                            Cantidad = data.Cantidad,
                            IngredientesArticulosPromocion = ingredientesPorArticulo,
                            TipoDePanesArticulosPromocion = data.TiposDePanArticulos
                        });
                    }

                }
                else
                {
                    resultado = 2;
                }


                var articlesOnCartSerielized = JsonConvert.SerializeObject(articulosVendidos);
                HttpContext.Session.SetString("ArticulosVendidos", articlesOnCartSerielized);

                return Json(resultado);
            }

            return Json(0);
            
        }
        [HttpPost]
        public JsonResult EliminarArticulo(string articuloID)
        {
            var articulos = getCartListSession();

            var articuloAEliminar = articulos.SingleOrDefault(a => a.ArticuloEnCarritoID.ToString() == articuloID);

            articulos.Remove(articuloAEliminar);

            var articlesOnCartSerielized = JsonConvert.SerializeObject(articulos);
            HttpContext.Session.SetString("ArticulosVendidos", articlesOnCartSerielized);



            var returnValues = new { nuevoTotal = articulos.Sum(a => a.Subtotal), isEmpty = articulos.Count == 0};

            return Json(returnValues);
        }

        public JsonResult GetAllArticulosEnCarrito()
        {
            var articlesOnCartRaw = HttpContext.Session.GetString("ArticulosVendidos");


            return Json(articlesOnCartRaw);
        }

        private List<ArticuloVendidoTempModel> getCartListSession()
        {
            var articlesOnCartRaw = HttpContext.Session.GetString("ArticulosVendidos");
            List<ArticuloVendidoTempModel> articulosVendidos = null;
            if (string.IsNullOrEmpty(articlesOnCartRaw))
            {
                articulosVendidos = new List<ArticuloVendidoTempModel>();
            }
            else
            {
                articulosVendidos = JsonConvert.DeserializeObject<List<ArticuloVendidoTempModel>>(articlesOnCartRaw);
            }

            return articulosVendidos;
        }
    }
}
