﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PIAInterfaz.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Controllers
{
    public class MenuController : Controller
    {
        private readonly ArticuloRepository articuloRepository;
        private readonly IngredienteRepository ingredienteRepository;

        public MenuController(ArticuloRepository articuloRepository, IngredienteRepository ingredienteRepository)
        {
            this.articuloRepository = articuloRepository;
            this.ingredienteRepository = ingredienteRepository;
        }
        private bool ShowOnboarding(string name)
        {
            bool showOnboarding = true;
            if (HttpContext.Session.GetString(name) != null)
                showOnboarding = false;
            else
                HttpContext.Session.SetString(name, "Visited");
            return showOnboarding;
        }

        [Route("/Menu/PizzasBasicas")]
        public async Task<IActionResult> Index()
        {
            var pizzasBasicas = await articuloRepository.getAllBasicPizzas();
           
            ViewBag.ShowOnboarding = ShowOnboarding("Menu-PizzasBasicas");
            return View("BasicPizzas", pizzasBasicas);
        }
        [Route("/Menu/PizzasEspeciales")]
        public async Task<IActionResult> Specials()
        {
            var pizzasEspeciales = await articuloRepository.getAllSpecialPizzas();
            var ingredientes = await ingredienteRepository.getAllIngredients();
            ViewBag.IngredientsList = ingredientes;
            ViewBag.ShowOnboarding = ShowOnboarding("Menu-Specials");
            return View("SpecialPizzas", pizzasEspeciales);
        }

        [Route("/Menu/Espaguetis")]
        public async Task<IActionResult> Spaghettis()
        {
            var listOfSpaghettis = await articuloRepository.getAllSpaghettis();
          
            ViewBag.ShowOnboarding = ShowOnboarding("Menu-Spaghettis");

            return View(listOfSpaghettis);
        }

        [Route("/Menu/Complementos")]
        public async Task<IActionResult> Complements()
        {
            var listOfComplements = await articuloRepository.getAllComplements();

            ViewBag.ShowOnboarding = ShowOnboarding("Menu-Complements");
            return View(listOfComplements);
        }
        [Route("Menu/Bebidas")]
        public async Task<IActionResult> Drinks()
        {
            var listOfDrinks = await articuloRepository.getAllDrinks();

            ViewBag.ShowOnboarding = ShowOnboarding("Menu-Drinks");
            return View(listOfDrinks);
        }


    }
}
