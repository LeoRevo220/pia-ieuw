﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using PIAInterfaz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PIAInterfaz.Controllers
{
    public class MiCuentaController : Controller
    {
        private readonly UserManager<Usuario> userManager;
        private readonly SignInManager<Usuario> signInManager;
        private readonly IWebHostEnvironment appEnvironment;
        private readonly IMailService mailService;

        public MiCuentaController(UserManager<Usuario> userManager, SignInManager<Usuario> signInManager, IMailService mailService, IWebHostEnvironment appEnvironment)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.mailService = mailService;
            this.appEnvironment = appEnvironment;
        }

        public async Task SendMail(MailRequestModel mail)
        {
            try
            {
                
                await mailService.SendEmailAsync(mail);
            } catch(Exception ex)
            {
                throw;
            }
        }

        public async Task SendWelcomeMail(WelcomeMailModel mail)
        {
            try
            {
                await mailService.SendWelcomeEmailAsync(mail, appEnvironment.WebRootPath);
            }catch(Exception ex)
            {
                throw;
            }
        }
       
        public IActionResult Index()
        {
            //return View("Login");
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("MiCuenta", "MiCuenta");
            }
            return RedirectToAction("Login");
            
            
        }
       
        [AcceptVerbs("Get", "Post")]
        public async Task<IActionResult> IsEmailInUse(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if(user == null)
            {
                return Json(true);
            }

            return Json($"El correo electronico {email} ya se encuentra en uso");
        }
        [HttpGet]
        [Route("Usuario")]
        public async Task<IActionResult> MiCuenta(bool showSuccessMessage = false)
        {
            var user = await signInManager.UserManager.GetUserAsync(User);

            var model = new AccountViewModel()
            {
                Name = user.Nombres,
                LastName = user.Apellidos,
                Phone = user.NumeroDeTelefono,
                Direccion = user.Direccion
            };
            ViewBag.ShowSuccessMessage = showSuccessMessage;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> EditarCuenta(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {   
                var user = await userManager.GetUserAsync(User);

                user.Nombres = model.Name;
                user.Apellidos = model.LastName;
                user.NumeroDeTelefono = model.Phone;
                user.Direccion = model.Direccion;

                try
                {

                    var result = await userManager.UpdateAsync(user);

                    if (!result.Succeeded)
                    {
                        return View("Error");
                    }

                    return RedirectToAction(nameof(MiCuenta), new { showSuccessMessage = true });
                } catch(Exception ex)
                {
                    return View("Error");
                }

                return RedirectToAction("MiCuenta");
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Inicio");
        }

        public async Task<IActionResult> Login(string returnUrl)
        {
            var model = new LoginViewModel()
            {
                ReturnUrl = returnUrl,
                ExternalLogin = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
            };
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("MiCuenta");
            }
            return View("Login", model);
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(model.Email, model.Password, isPersistent: false, false);

                if (result.Succeeded)
                {
                    if(Url.IsLocalUrl(returnUrl) && !string.IsNullOrEmpty(returnUrl))
                    {
                        return LocalRedirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Inicio");
                    }
                }

                ModelState.AddModelError("", "Usuario o contraseña incorrectos");
            }
            model.ReturnUrl = returnUrl;
            model.ExternalLogin = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult ExternalLogin(string provider, string returnUrl)
        {
            var redirectUrl = Url.Action("ExternalLoginCallback", "MiCuenta", new { ReturnUrl = returnUrl });

            var properties = signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);

            return new ChallengeResult(provider, properties);
        }

        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            var model = new LoginViewModel()
            {
                ReturnUrl = returnUrl,
                ExternalLogin = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList()
            };

            if(remoteError != null)
            {
                ModelState.AddModelError(string.Empty, $"Ha ocurrido un error con Facebook: {remoteError}");

                return View("Login", model);
            }

            var info = await signInManager.GetExternalLoginInfoAsync();
            if(info == null)
            {
                ModelState.AddModelError(string.Empty, "Ha ocurrido un error al cargar la informacion del usuario de Facebook");

                return View("Login", model);
            }

            var result = await signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);

            if (result.Succeeded)
            {
                return LocalRedirect(returnUrl);
            } else
            {
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                if(email != null)
                {
                    var user = await userManager.FindByEmailAsync(email);

                    if(user == null)
                    {
                        user = new Usuario()
                        {
                            UserName = info.Principal.FindFirstValue(ClaimTypes.Email),
                            Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                            Nombres = info.Principal.FindFirstValue(ClaimTypes.Name).Split(" ")[0],
                            Apellidos = info.Principal.FindFirstValue(ClaimTypes.Name).Split(" ")[1]

                        };

                        await userManager.CreateAsync(user);
                    }

                    await userManager.AddLoginAsync(user, info);
                    await signInManager.SignInAsync(user, isPersistent: false);

                    return LocalRedirect(returnUrl);

                }

                return View("Error");
            }
        }

        [Route("Registrarse")]
        public IActionResult Register()
        {
            return View("Register");
        }

        [HttpPost]
        [Route("Registrarse")]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var newUser = new Usuario()
                {
                    Nombres = model.Name,
                    Apellidos = model.LastName,
                    Email = model.Email,
                    UserName = model.Email,
                    NumeroDeTelefono = model.Telefono
                };

                var result = await userManager.CreateAsync(newUser, model.Password);

                if (result.Succeeded)
                {
                  

                    await SendWelcomeMail(new WelcomeMailModel()
                    {
                        ToEmail = model.Email,
                        UserName = model.Name + " " + model.LastName
                    });
                    try
                    {
                        await signInManager.SignInAsync(newUser, isPersistent: false);
                    }catch(Exception ex)
                    {
                        //TODO: Send error message to the view
                    }
                    return RedirectToAction("Index", "Inicio");
                }


                if (result.Errors.Count() > 0)
                {
                    ModelState.AddModelError(string.Empty, "La contraseña debe contener al menos una letra minuscula, una mayuscula, un numero, un caracter no" +
                        "alfanumerico y la longitud debe ser minimo de 6 caracteres");

                }
                    
                   
                

            }
            return View(model);
        }
    }
}
