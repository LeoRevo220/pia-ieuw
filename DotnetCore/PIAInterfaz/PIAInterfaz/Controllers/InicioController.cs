﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using PIAInterfaz.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Controllers
{
    public class InicioController : Controller
    {
        private readonly ILogger<InicioController> _logger;

        private readonly SignInManager<Usuario> signInManager;

        private readonly ArticuloRepository articuloRepository;

        public InicioController(ILogger<InicioController> logger, ArticuloRepository articuloRepository, SignInManager<Usuario> signInManager)
        {
            this.articuloRepository = articuloRepository;
            this.signInManager = signInManager;
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            //TESTING SESSION
            HttpContext.Session.SetInt32("ClaveChida", 390);


            var pizzasEspeciales = await articuloRepository.getAllSpecialPizzas();
            var listOfPizzasEspeciales = pizzasEspeciales.GetRange(0, 4);

            var model = new InicioViewModel
            {
                PizzasEspeciales = listOfPizzasEspeciales
            };
            if (User.Identity.IsAuthenticated)
            {
                var listOfTopSellers = await articuloRepository.getTopSellers();
                model.MasVendidas = listOfTopSellers;

                var userLoggedInId = signInManager.UserManager.GetUserId(User);
                var listOfRecentArticles = await articuloRepository.getRecentArticlesByUser(userLoggedInId);

                model.VuelveAOrdenar = listOfRecentArticles;
            }




            return View(model);
        }

        [Route("/Inicio/Error/{code:int}")]
        public IActionResult Error(int code)
        {
            ViewBag.ErrorMessage = $"Ha ocurrido un error, codigo de error: {code}";
            ViewBag.CodeError = code;
            return View("Error");
        }
    }
}
