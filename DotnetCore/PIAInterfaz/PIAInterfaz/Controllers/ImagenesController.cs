﻿using Microsoft.AspNetCore.Mvc;
using PIAInterfaz.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Controllers
{
    public class ImagenesController : Controller
    {
        private readonly ImagenRepository imagenRepository;
        public ImagenesController(ImagenRepository imagenRepository)
        {
            this.imagenRepository = imagenRepository;
        }

        public async Task<IActionResult> GetImage(int id)
        {
            var imagenData = await imagenRepository.getImageById(id);



            return File(imagenData, "image/jpg");
        }

    }
}
