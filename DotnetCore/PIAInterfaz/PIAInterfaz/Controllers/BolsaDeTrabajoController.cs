﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PIAInterfaz.Data;
using PIAInterfaz.Models;
using PIAInterfaz.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PIAInterfaz.Controllers
{

 
    public class BolsaDeTrabajoController : Controller
    {
        private readonly UserManager<Usuario> userManager;
        private readonly MensajeRepository mensajeRepository;
        public BolsaDeTrabajoController(UserManager<Usuario> userManager, MensajeRepository mensajeRepository)
        {
            this.userManager = userManager;
            this.mensajeRepository = mensajeRepository;
        }
        public async Task<IActionResult> Index(bool showSuccessMessage = false)
        {

            var model = new ContactViewModel();
            if (User.Identity.IsAuthenticated)
            {
                var user = await userManager.GetUserAsync(User);
                model.Nombre = user.Nombres + " " + user.Apellidos;
                model.Email = user.Email;
            }
            ViewBag.ShowSuccessMessage = showSuccessMessage;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SendOpinion(ContactViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await mensajeRepository.AddMessage(model);
                if (result != 0)
                {
               
                    return RedirectToAction(nameof(Index), new { showSuccessMessage = true });
                }
                return View("Error");
            }

            return View(model);
        }
    }
}
