﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class tableImagesMaked : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ImagenID",
                table: "Articulos",
                type: "int",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Imagenes",
                columns: table => new
                {
                    ImagenID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ImagenData = table.Column<byte[]>(type: "varbinary(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imagenes", x => x.ImagenID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articulos_ImagenID",
                table: "Articulos",
                column: "ImagenID");

            migrationBuilder.AddForeignKey(
                name: "FK_Articulos_Imagenes_ImagenID",
                table: "Articulos",
                column: "ImagenID",
                principalTable: "Imagenes",
                principalColumn: "ImagenID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articulos_Imagenes_ImagenID",
                table: "Articulos");

            migrationBuilder.DropTable(
                name: "Imagenes");

            migrationBuilder.DropIndex(
                name: "IX_Articulos_ImagenID",
                table: "Articulos");

            migrationBuilder.DropColumn(
                name: "ImagenID",
                table: "Articulos");
        }
    }
}
