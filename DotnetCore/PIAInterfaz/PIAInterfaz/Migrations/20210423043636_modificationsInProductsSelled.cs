﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class modificationsInProductsSelled : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articulos_Imagenes_ImagenID",
                table: "Articulos");

            migrationBuilder.DropTable(
                name: "ArticuloCompra");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Fecha",
                table: "Compra",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<int>(
                name: "ImagenID",
                table: "Articulos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "ArticulosVendidos",
                columns: table => new
                {
                    ArticuloVendidoID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Precio = table.Column<float>(type: "real", nullable: false),
                    Nombre = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ArticuloID = table.Column<int>(type: "int", nullable: false),
                    CompraID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticulosVendidos", x => x.ArticuloVendidoID);
                    table.ForeignKey(
                        name: "FK_ArticulosVendidos_Articulos_ArticuloID",
                        column: x => x.ArticuloID,
                        principalTable: "Articulos",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArticulosVendidos_Compra_CompraID",
                        column: x => x.CompraID,
                        principalTable: "Compra",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArticulosVendidos_ArticuloID",
                table: "ArticulosVendidos",
                column: "ArticuloID");

            migrationBuilder.CreateIndex(
                name: "IX_ArticulosVendidos_CompraID",
                table: "ArticulosVendidos",
                column: "CompraID");

            migrationBuilder.AddForeignKey(
                name: "FK_Articulos_Imagenes_ImagenID",
                table: "Articulos",
                column: "ImagenID",
                principalTable: "Imagenes",
                principalColumn: "ImagenID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articulos_Imagenes_ImagenID",
                table: "Articulos");

            migrationBuilder.DropTable(
                name: "ArticulosVendidos");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Fecha",
                table: "Compra",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ImagenID",
                table: "Articulos",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "ArticuloCompra",
                columns: table => new
                {
                    ArticulosID = table.Column<int>(type: "int", nullable: false),
                    ComprasID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticuloCompra", x => new { x.ArticulosID, x.ComprasID });
                    table.ForeignKey(
                        name: "FK_ArticuloCompra_Articulos_ArticulosID",
                        column: x => x.ArticulosID,
                        principalTable: "Articulos",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArticuloCompra_Compra_ComprasID",
                        column: x => x.ComprasID,
                        principalTable: "Compra",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArticuloCompra_ComprasID",
                table: "ArticuloCompra",
                column: "ComprasID");

            migrationBuilder.AddForeignKey(
                name: "FK_Articulos_Imagenes_ImagenID",
                table: "Articulos",
                column: "ImagenID",
                principalTable: "Imagenes",
                principalColumn: "ImagenID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
