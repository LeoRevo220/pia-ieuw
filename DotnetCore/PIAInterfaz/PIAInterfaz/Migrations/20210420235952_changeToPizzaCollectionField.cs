﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class changeToPizzaCollectionField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articulos_Ingredientes_IngredienteID",
                table: "Articulos");

            migrationBuilder.DropForeignKey(
                name: "FK_Ingredientes_Articulos_PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropIndex(
                name: "IX_Ingredientes_PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropIndex(
                name: "IX_Articulos_IngredienteID",
                table: "Articulos");

            migrationBuilder.DropColumn(
                name: "PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropColumn(
                name: "IngredienteID",
                table: "Articulos");

            migrationBuilder.CreateTable(
                name: "IngredientePizza",
                columns: table => new
                {
                    IngredientesID = table.Column<int>(type: "int", nullable: false),
                    PizzasID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngredientePizza", x => new { x.IngredientesID, x.PizzasID });
                    table.ForeignKey(
                        name: "FK_IngredientePizza_Articulos_PizzasID",
                        column: x => x.PizzasID,
                        principalTable: "Articulos",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IngredientePizza_Ingredientes_IngredientesID",
                        column: x => x.IngredientesID,
                        principalTable: "Ingredientes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IngredientePizza_PizzasID",
                table: "IngredientePizza",
                column: "PizzasID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IngredientePizza");

            migrationBuilder.AddColumn<int>(
                name: "PizzaID",
                table: "Ingredientes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IngredienteID",
                table: "Articulos",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ingredientes_PizzaID",
                table: "Ingredientes",
                column: "PizzaID");

            migrationBuilder.CreateIndex(
                name: "IX_Articulos_IngredienteID",
                table: "Articulos",
                column: "IngredienteID");

            migrationBuilder.AddForeignKey(
                name: "FK_Articulos_Ingredientes_IngredienteID",
                table: "Articulos",
                column: "IngredienteID",
                principalTable: "Ingredientes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ingredientes_Articulos_PizzaID",
                table: "Ingredientes",
                column: "PizzaID",
                principalTable: "Articulos",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
