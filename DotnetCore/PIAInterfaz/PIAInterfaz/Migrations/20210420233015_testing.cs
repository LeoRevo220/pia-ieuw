﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class testing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articulo_Articulo_PromocionID",
                table: "Articulo");

            migrationBuilder.DropForeignKey(
                name: "FK_ArticuloCompra_Articulo_ArticulosID",
                table: "ArticuloCompra");

            migrationBuilder.DropTable(
                name: "IngredientePizza");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Articulo",
                table: "Articulo");

            migrationBuilder.RenameTable(
                name: "Articulo",
                newName: "Articulos");

            migrationBuilder.RenameIndex(
                name: "IX_Articulo_PromocionID",
                table: "Articulos",
                newName: "IX_Articulos_PromocionID");

            migrationBuilder.AddColumn<int>(
                name: "PizzaID",
                table: "Ingredientes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "IngredienteID",
                table: "Articulos",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Articulos",
                table: "Articulos",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_Ingredientes_PizzaID",
                table: "Ingredientes",
                column: "PizzaID");

            migrationBuilder.CreateIndex(
                name: "IX_Articulos_IngredienteID",
                table: "Articulos",
                column: "IngredienteID");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticuloCompra_Articulos_ArticulosID",
                table: "ArticuloCompra",
                column: "ArticulosID",
                principalTable: "Articulos",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Articulos_Articulos_PromocionID",
                table: "Articulos",
                column: "PromocionID",
                principalTable: "Articulos",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Articulos_Ingredientes_IngredienteID",
                table: "Articulos",
                column: "IngredienteID",
                principalTable: "Ingredientes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ingredientes_Articulos_PizzaID",
                table: "Ingredientes",
                column: "PizzaID",
                principalTable: "Articulos",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticuloCompra_Articulos_ArticulosID",
                table: "ArticuloCompra");

            migrationBuilder.DropForeignKey(
                name: "FK_Articulos_Articulos_PromocionID",
                table: "Articulos");

            migrationBuilder.DropForeignKey(
                name: "FK_Articulos_Ingredientes_IngredienteID",
                table: "Articulos");

            migrationBuilder.DropForeignKey(
                name: "FK_Ingredientes_Articulos_PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropIndex(
                name: "IX_Ingredientes_PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Articulos",
                table: "Articulos");

            migrationBuilder.DropIndex(
                name: "IX_Articulos_IngredienteID",
                table: "Articulos");

            migrationBuilder.DropColumn(
                name: "PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropColumn(
                name: "IngredienteID",
                table: "Articulos");

            migrationBuilder.RenameTable(
                name: "Articulos",
                newName: "Articulo");

            migrationBuilder.RenameIndex(
                name: "IX_Articulos_PromocionID",
                table: "Articulo",
                newName: "IX_Articulo_PromocionID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Articulo",
                table: "Articulo",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "IngredientePizza",
                columns: table => new
                {
                    IngredientesID = table.Column<int>(type: "int", nullable: false),
                    PizzasID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngredientePizza", x => new { x.IngredientesID, x.PizzasID });
                    table.ForeignKey(
                        name: "FK_IngredientePizza_Articulo_PizzasID",
                        column: x => x.PizzasID,
                        principalTable: "Articulo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IngredientePizza_Ingredientes_IngredientesID",
                        column: x => x.IngredientesID,
                        principalTable: "Ingredientes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IngredientePizza_PizzasID",
                table: "IngredientePizza",
                column: "PizzasID");

            migrationBuilder.AddForeignKey(
                name: "FK_Articulo_Articulo_PromocionID",
                table: "Articulo",
                column: "PromocionID",
                principalTable: "Articulo",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ArticuloCompra_Articulo_ArticulosID",
                table: "ArticuloCompra",
                column: "ArticulosID",
                principalTable: "Articulo",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
