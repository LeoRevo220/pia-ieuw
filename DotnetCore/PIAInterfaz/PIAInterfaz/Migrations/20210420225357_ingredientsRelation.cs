﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class ingredientsRelation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ingredientes_Articulo_PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropForeignKey(
                name: "FK_Ingredientes_Articulo_PizzaID1",
                table: "Ingredientes");

            migrationBuilder.DropIndex(
                name: "IX_Ingredientes_PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropIndex(
                name: "IX_Ingredientes_PizzaID1",
                table: "Ingredientes");

            migrationBuilder.DropColumn(
                name: "PizzaID",
                table: "Ingredientes");

            migrationBuilder.DropColumn(
                name: "PizzaID1",
                table: "Ingredientes");

            migrationBuilder.CreateTable(
                name: "IngredientePizza",
                columns: table => new
                {
                    IngredientesID = table.Column<int>(type: "int", nullable: false),
                    PizzasID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IngredientePizza", x => new { x.IngredientesID, x.PizzasID });
                    table.ForeignKey(
                        name: "FK_IngredientePizza_Articulo_PizzasID",
                        column: x => x.PizzasID,
                        principalTable: "Articulo",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_IngredientePizza_Ingredientes_IngredientesID",
                        column: x => x.IngredientesID,
                        principalTable: "Ingredientes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_IngredientePizza_PizzasID",
                table: "IngredientePizza",
                column: "PizzasID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IngredientePizza");

            migrationBuilder.AddColumn<int>(
                name: "PizzaID",
                table: "Ingredientes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "PizzaID1",
                table: "Ingredientes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ingredientes_PizzaID",
                table: "Ingredientes",
                column: "PizzaID");

            migrationBuilder.CreateIndex(
                name: "IX_Ingredientes_PizzaID1",
                table: "Ingredientes",
                column: "PizzaID1");

            migrationBuilder.AddForeignKey(
                name: "FK_Ingredientes_Articulo_PizzaID",
                table: "Ingredientes",
                column: "PizzaID",
                principalTable: "Articulo",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ingredientes_Articulo_PizzaID1",
                table: "Ingredientes",
                column: "PizzaID1",
                principalTable: "Articulo",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
