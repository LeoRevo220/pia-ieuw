﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class addedTwoFieldsToCompraTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Direccion",
                table: "Compras",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MetodoDePago",
                table: "Compras",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Direccion",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "MetodoDePago",
                table: "Compras");
        }
    }
}
