﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class addedRelationBetweenIngredientsAndSelledArticles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ArticuloVendidoIngrediente",
                columns: table => new
                {
                    ArticuloVendidosArticuloVendidoID = table.Column<int>(type: "int", nullable: false),
                    IngredientesID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArticuloVendidoIngrediente", x => new { x.ArticuloVendidosArticuloVendidoID, x.IngredientesID });
                    table.ForeignKey(
                        name: "FK_ArticuloVendidoIngrediente_ArticulosVendidos_ArticuloVendidosArticuloVendidoID",
                        column: x => x.ArticuloVendidosArticuloVendidoID,
                        principalTable: "ArticulosVendidos",
                        principalColumn: "ArticuloVendidoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArticuloVendidoIngrediente_Ingredientes_IngredientesID",
                        column: x => x.IngredientesID,
                        principalTable: "Ingredientes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArticuloVendidoIngrediente_IngredientesID",
                table: "ArticuloVendidoIngrediente",
                column: "IngredientesID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArticuloVendidoIngrediente");
        }
    }
}
