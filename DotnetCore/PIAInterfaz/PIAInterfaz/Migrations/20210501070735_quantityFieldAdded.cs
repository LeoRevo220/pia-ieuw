﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class quantityFieldAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticulosVendidos_Compra_CompraID",
                table: "ArticulosVendidos");

            migrationBuilder.DropForeignKey(
                name: "FK_Compra_AspNetUsers_UsuarioId",
                table: "Compra");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Compra",
                table: "Compra");

            migrationBuilder.RenameTable(
                name: "Compra",
                newName: "Compras");

            migrationBuilder.RenameIndex(
                name: "IX_Compra_UsuarioId",
                table: "Compras",
                newName: "IX_Compras_UsuarioId");

            migrationBuilder.AlterColumn<int>(
                name: "CompraID",
                table: "ArticulosVendidos",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Cantidad",
                table: "ArticulosVendidos",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UserID",
                table: "Compras",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Compras",
                table: "Compras",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticulosVendidos_Compras_CompraID",
                table: "ArticulosVendidos",
                column: "CompraID",
                principalTable: "Compras",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Compras_AspNetUsers_UsuarioId",
                table: "Compras",
                column: "UsuarioId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ArticulosVendidos_Compras_CompraID",
                table: "ArticulosVendidos");

            migrationBuilder.DropForeignKey(
                name: "FK_Compras_AspNetUsers_UsuarioId",
                table: "Compras");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Compras",
                table: "Compras");

            migrationBuilder.DropColumn(
                name: "Cantidad",
                table: "ArticulosVendidos");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Compras");

            migrationBuilder.RenameTable(
                name: "Compras",
                newName: "Compra");

            migrationBuilder.RenameIndex(
                name: "IX_Compras_UsuarioId",
                table: "Compra",
                newName: "IX_Compra_UsuarioId");

            migrationBuilder.AlterColumn<int>(
                name: "CompraID",
                table: "ArticulosVendidos",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Compra",
                table: "Compra",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_ArticulosVendidos_Compra_CompraID",
                table: "ArticulosVendidos",
                column: "CompraID",
                principalTable: "Compra",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Compra_AspNetUsers_UsuarioId",
                table: "Compra",
                column: "UsuarioId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
