﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PIAInterfaz.Migrations
{
    public partial class tipoDePanFieldAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "TipoDePan",
                table: "ArticulosVendidos",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TipoDePan",
                table: "ArticulosVendidos");
        }
    }
}
